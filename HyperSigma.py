#=============================================================
# Imports 

import os
import subprocess
import shutil
from typing import Literal
from nicegui import ui
from datetime import datetime

#=============================================================



#=============================================================
# Setup
port = 8888
default_ip_address = "192.168.1.129"

#=============================================================



#=============================================================
# MAIN
#=============================================================

# Logo
logo_zoom = 0.3
ui.image("source/logo.png").props(f"width={1558 * logo_zoom}px height={276 * logo_zoom}px").force_reload()


with ui.row():
    # Input IP address & filename to be saved
    ip_address = ui.input(label="IP Address", value=default_ip_address)
    filename = ui.input(label="Filename", placeholder='start typing')

with ui.row():
    # Buttons to get or save picture
    get_picture = ui.button("Get Picture", on_click=lambda: load_picture())
    save_picture = ui.button("Save Picture", on_click=lambda: save_picture())

with ui.row():
    # Copy file with new filename
    shutil.copyfile("source/test_picture.png", "dump.png")
    screen = ui.image("dump.png").props(f"width={800}px height={480}px")
    screen.force_reload()

# Run UI
ui.run(title="Hyper Sigma", port=port)

#=============================================================



# =============================================================
# Functions to use in the actual code

def load_picture():
    # Load picture by calling rigol_grap
    return_code = os.system("python rigol-grab/rigol_grab.py --port" + " " + ip_address.value + " " + "--filename dump.png")

    if return_code == 0:
        # Show screen when command successfull
        ui.notify("Loading...", position="bottom-left", type="positive")
        screen.force_reload()
    else:
        # Error
        ui.notify("Could not load screen", position="bottom-left", type="negative")




def save_picture():
    # Generate date & time for save
    tmp_filename = datetime.now().strftime("%Y%m%d_%H%M%S")
    if not filename.value == "":
        # If filename was entered, append to datetime
        tmp_filename = tmp_filename + "_" + filename.value
    # Copy file with new filename
    shutil.copyfile("dump.png", "scope_pictures/" + tmp_filename + ".png")
    ui.notify("Saving...")

# =============================================================