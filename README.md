# HyperSigma

## Introduction
Small GUI Tool to load and show pictures with the help of [rigol_grab](https://github.com/rdpoor/rigol-grab).


## Installation
Please follow those commands. Already installed packages can be ignored.

```
python -m pip install pipenv
python -m pip install nicegui
git clone https://gitlab.com/BenjiBango/hypersigma
cd <into your hypersigma folder>
git clone https://github.com/rdpoor/rigol-grab.git
cd rigol-grab
pipenv install
```

## Starting
Just execute hypersigma.py however you see fit (double click, via cmd, etc.), after this, a website should start in which the IP-Address of the Rigol Sopce can be entered, and a name for a savefile.

## Known bugs
- Exiting the website won't close the python script
